package view;

import controller.Controller;
import view.listeners.*;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;


/**
 * Created by romanletun on 26.05.2016.
 * основное окно приложения.
 */
public class View extends JFrame implements ActionListener
{
    private JButton buttonRemoveUnUsedStyles = new JButton(); //Кнопка удалить неиспользуемые стили
    private JButton buttonSelectLeftFile = new JButton(); //Левая кнопка "Открыть файл"
    private JButton buttonDeleteStylesLeftFile = new JButton(); //Кнопка "Удалить выбранные стили"
    //private JButton buttonDeleteStylesLeftFile = new JButton();
    private JButton buttonSelectRightFile = new JButton(); //Правая кнопка "Открыть файл"
    private JButton buttonStyleReplace = new JButton(); //Кнопка "заменить стили"
    private JButton buttonSaveDocument = new JButton(); //Кнопка "Сохранить документ"
    private JList<String> listLeftStyles = new JList<>(); //Лист, для отображения стилей левого документа
    private JList<String> listRightStyles = new JList<>(); //Лист, для отображения стилей правого документа
    private JTable table; //таблица, с помощью которой задаётся соответствие заменяемых стилей
    private DefaultTableModel dm = new DefaultTableModel();
    private JLabel LabelLeftDoc = new JLabel("Документ для замены стилей");
    private JLabel LabelRightDoc = new JLabel("Документ донор стилей");
    private Controller controller;

    public View(Controller controller) throws HeadlessException
    {
        this.controller = controller;
    }

    //Обработчики нажатий кнопок меню
    @Override
    public void actionPerformed(ActionEvent e)
    {
        switch (e.getActionCommand())
        {
            case "Загрузить файл настроек":
                controller.importConfig();
                break;
            case "Сохранить настройки":
                controller.saveConfig(getStylesMap(), getRightStyles());
                break;
            case "Выход":
                exit();
                break;
            case "О программе":
                showAbout();
                break;
        }
    }

    //Инициализация меню
    public void initMenuBar()
    {
        JMenuBar menuBar = new JMenuBar();

        MenuHelper.initFileMenu(this, menuBar);
        MenuHelper.initHelpMenu(this, menuBar);

        getContentPane().add(menuBar, BorderLayout.NORTH);
    }

    //Обновление таблицы в соответствии с загруженым конфиг файлом
    public void updateTableForImport(Map<String, String> replacedMap)
    {
        for (int i = 0; i < dm.getRowCount(); i++)
        {
            dm.removeRow(i);
        }

        for (Map.Entry<String, String> map : replacedMap.entrySet())
        {
            dm.addRow(new String[]{map.getKey(), map.getValue()});
        }
    }

    //Инициализация таблицы
    public void tableInit()
    {
        dm.setDataVector(new Object[][]{{ "", "" }}, new Object[] { "Заменяемый стиль", "Стиль для замены" });

        table = new JTable(dm);

        //tableRefresh();

        JPanel panel2 = new JPanel();
        panel2.setLayout(new BorderLayout());
        panel2.setPreferredSize(new Dimension(400, 500));

        panel2.add(new Label("Выберите стили для замены."), BorderLayout.NORTH);
        panel2.add(new JScrollPane(table), BorderLayout.BEFORE_FIRST_LINE);
        panel2.add(new JSplitPane(JSplitPane.VERTICAL_SPLIT, buttonSaveDocument, buttonStyleReplace), BorderLayout.SOUTH);

        getContentPane().add(panel2, BorderLayout.CENTER);

        pack();
    }

    //Устанавливает CellEditor( ComboBox ) для колонок таблицы.
    private void tableRefresh()
    {
        table.getColumn("Заменяемый стиль").setCellEditor(new DefaultCellEditor(getNewCombo(Side.RIGHT)));
        table.getColumn("Стиль для замены").setCellEditor(new DefaultCellEditor(getNewCombo(Side.LEFT)));

        table.getColumn("Заменяемый стиль").getCellEditor().addCellEditorListener(new CellEditorListener() {
            @Override
            public void editingStopped(ChangeEvent e) {
                tableRefresh();
                if (dm.getRowCount() == getStylesMap().size()) {
                    dm.addRow(new Object[][]{{"", ""}});
                    dm.setValueAt("", dm.getRowCount() - 1, 0);
                }
            }

            @Override
            public void editingCanceled(ChangeEvent e) {

            }
        });
    }

    //Получает Map'у "Заменяемый стиль" --> "Стиль для замены"
    private Map<String, String> getStylesMap()
    {
        Map<String, String> resultMap = new HashMap<>();

        String emptyString = "";

        for (int i = 0; i < dm.getRowCount(); i++)
        {
            if (dm.getValueAt(i, 0) != null && !"".equals(dm.getValueAt(i, 0).toString())) {
                if (dm.getValueAt(i, 1) != null)
                    resultMap.put(dm.getValueAt(i, 0).toString(), dm.getValueAt(i, 1).toString());
                else
                    resultMap.put(dm.getValueAt(i, 0).toString(), emptyString);
            }
        }

        return resultMap;
    }

    private List<String> getRightStyles()
    {
        return new ArrayList<>(controller.getRightStyles());
    }

    //Обновляет список доступных для выбора значений в ComboBox
    private JComboBox<String> getNewCombo(Side side)
    {
        JComboBox<String> newComboBox = new JComboBox<>();

        List<String> styles = new ArrayList<>();

        if (side == Side.RIGHT) {
            styles.addAll(controller.getRightStyles());

            Map<String,String> map = getStylesMap();

            if (map.size() > 0)
            {
                List<String> selected = new ArrayList<>(map.keySet());
                styles.removeAll(selected);
            }
        }
        else
            styles.addAll(controller.getLeftStyles());

        for (String s : styles)
        {
            newComboBox.addItem(s);
        }

        return newComboBox;
    }

    //Инициализация View
    public void init()
    {
        buttonSelectLeftFile.setText("Открыть файл.");
        buttonDeleteStylesLeftFile.setText("Удалить выбранные стили.");
        buttonSelectRightFile.setText("Открыть файл.");
        //buttonDeleteStylesLeftFile.setText("Удалить выбранные стили.");
        buttonStyleReplace.setText("Заменить стили.");
        buttonSaveDocument.setText("Сохранить документ.");
        buttonRemoveUnUsedStyles.setText("Удалить неиспользуемые стили");

        buttonRemoveUnUsedStyles.addMouseListener(new RemoveUnUsedStylesListener(this));

        buttonStyleReplace.addMouseListener(new ReplaceButtonListener(this));
        buttonSaveDocument.addMouseListener(new SaveDocButtonListener(this));

        buttonSelectLeftFile.addMouseListener(new SelectFileButtonListener(this, Side.LEFT));
        buttonSelectRightFile.addMouseListener(new SelectFileButtonListener(this, Side.RIGHT));

        buttonDeleteStylesLeftFile.addMouseListener(new DeleteStyleButtonListener(this));
        //buttonDeleteStylesLeftFile.addMouseListener(new DeleteStyleButtonListener(this, Side.LEFT));

        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BorderLayout());

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        panel1.setPreferredSize(new Dimension(230, 100));

        panel1.add(LabelLeftDoc, BorderLayout.NORTH);
        panel1.add(new JScrollPane(listLeftStyles), BorderLayout.CENTER);
        panel1.add(buttonSelectLeftFile, BorderLayout.SOUTH);
        //panel1.add(buttonRemoveUnUsedStyles, BorderLayout.AFTER_LAST_LINE);

        JPanel panel3 = new JPanel();
        panel3.setLayout(new BorderLayout());
        panel3.setPreferredSize(new Dimension(230, 100));

        panel3.add(LabelRightDoc, BorderLayout.NORTH);
        panel3.add(new JScrollPane(listRightStyles), BorderLayout.CENTER);
        panel3.add(buttonSelectRightFile, BorderLayout.SOUTH);

        tableInit();

        leftPanel.add(panel1, BorderLayout.CENTER);
        leftPanel.add(new JSplitPane(JSplitPane.VERTICAL_SPLIT, buttonDeleteStylesLeftFile, buttonRemoveUnUsedStyles), BorderLayout.SOUTH);

        getContentPane().add(leftPanel, BorderLayout.WEST);
        getContentPane().add(panel3, BorderLayout.EAST);

        initMenuBar();

        setLocationRelativeTo(null);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void removeUnUsedStyles()
    {
        controller.removeUnUsedStyles();
    }

    public void resetListLeftStyles(List<String> styles)
    {
        String[] strings = new String[styles.size()];

        listLeftStyles.setListData(styles.toArray(strings));
    }

    public void resetListRightStyles(List<String> styles)
    {
        String[] strings = new String[styles.size()];

        listRightStyles.setListData(styles.toArray(strings));
    }

    public void selectFile(Side button)
    {
        controller.selectFile(button);
        controller.getStylesFromModel(button);

        tableRefresh();
    }

    public void deleteStyles()
    {
        List<String> stylesForDelete = listLeftStyles.getSelectedValuesList();

        controller.deleteStyles(stylesForDelete);
    }

    public void saveFile()
    {
        controller.saveFile();
    }

    public void replaceStyles()
    {
        controller.replaceStyles(getStylesMap());
    }

    private void exit()
    {
        controller.exit();
    }

    private void showAbout()
    {
        String about = "StyleReplacer, 2016. (c) Roman Letunovskii.\n" +
                "Программа предназначена для работы со стилями документа Word.\n" +
                "Работает с двумя документами (левый и правый)\n" +
                "Для левого документа позволяет:\n" +
                "1. Удалять выделеные стили\n" +
                "2. Удалять неиспользуемые стили\n" +
                "Так же позволяет заменять стили в левом документе стилями из правого документа.\n" +
                "Соответствие заменяемых стилей задаётся с пощью таблицы.\n" +
                "Данные этой таблицы так же могут быть сохранены в .XML файл и быть использованы впоследствии.";

        JOptionPane.showMessageDialog(getContentPane(), about, "О программе", JOptionPane.INFORMATION_MESSAGE);
    }
}
