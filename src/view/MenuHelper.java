package view;

/**
 * Created by romanletun on 31.05.2016.
 */
import javax.swing.*;
import java.awt.event.ActionListener;

public class MenuHelper {
    public static JMenuItem addMenuItem(JMenu parent, String text, ActionListener actionListener) {
        JMenuItem menuItem = new JMenuItem(text);
        menuItem.addActionListener(actionListener);
        parent.add(menuItem);
        return menuItem;
    }

    public static void initHelpMenu(View view, JMenuBar menuBar) {
        JMenu helpMenu = new JMenu("Помощь");
        menuBar.add(helpMenu);

        addMenuItem(helpMenu, "О программе", view);
    }


    public static void initFileMenu(View view, JMenuBar menuBar) {
        JMenu fileMenu = new JMenu("Файл");
        menuBar.add(fileMenu);

        //addMenuItem(fileMenu, "Загрузить файл настроек", view);
        addMenuItem(fileMenu, "Загрузить файл настроек", view);
        addMenuItem(fileMenu, "Сохранить настройки", view);
        fileMenu.addSeparator();
        addMenuItem(fileMenu, "Выход", view);
    }
}