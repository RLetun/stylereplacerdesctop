package view.listeners;

import view.Side;
import view.View;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by romanletun on 26.05.2016.
 */
public class SelectFileButtonListener implements MouseListener
{
    private View view;
    private Side button;

    public SelectFileButtonListener(View view, Side button)
    {
        this.view = view;
        this.button = button;
    }

    @Override
    public void mouseClicked(MouseEvent e)
    {
        view.selectFile(button);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
