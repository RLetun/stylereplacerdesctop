package view.listeners;

import view.View;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by romanletun on 31.05.2016.
 */
public class RemoveUnUsedStylesListener implements MouseListener
{
    private View view;

    public RemoveUnUsedStylesListener(View view) {
        this.view = view;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        view.removeUnUsedStyles();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
