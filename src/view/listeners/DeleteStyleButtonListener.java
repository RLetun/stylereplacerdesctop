package view.listeners;

import view.Side;
import view.View;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by romanletun on 27.05.2016.
 */
public class DeleteStyleButtonListener implements MouseListener
{
    private View view;

    public DeleteStyleButtonListener(View view) {
        this.view = view;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        view.deleteStyles();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
