package view.listeners;

import view.View;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by romanletun on 30.05.2016.
 */
public class ReplaceButtonListener implements MouseListener
{
    private View view;

    public ReplaceButtonListener(View view) {
        this.view = view;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        view.replaceStyles();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
