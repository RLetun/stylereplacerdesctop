package model;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by romanletun on 26.05.2016.
 * Класс для работы с XML-ыми файлами word
 */
public abstract class WordParser
{
    private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    private static List<String> getParentStyles(Set<String> styles, Document doc)
    {
        List<String> result = new ArrayList<>();

        NodeList nodeList = doc.getDocumentElement().getChildNodes();

        for (String s : styles)
        {
            String styleName = s;
            //String numericStyle = "";
            do {
                for (int i = 0; i < nodeList.getLength(); i++)
                {
                    if (((Element) nodeList.item(i)).getTagName().equalsIgnoreCase("w:style"))
                    {
                        if (((Element) nodeList.item(i).getFirstChild()).getAttribute("w:val").equalsIgnoreCase(styleName)
                                || ((Element) nodeList.item(i)).getAttribute("w:styleId").equalsIgnoreCase(styleName))
                        {
                            Node node = nodeList.item(i);

                            if (!result.contains(((Element) nodeList.item(i).getFirstChild()).getAttribute("w:val")))
                                result.add(((Element) nodeList.item(i).getFirstChild()).getAttribute("w:val"));

                            styleName = "";

                            for (int j = 0; j < node.getChildNodes().getLength(); j++)
                            {
                                if (((Element) node.getChildNodes().item(j)).getTagName().equalsIgnoreCase("w:basedOn"))
                                    styleName = ((Element) node.getChildNodes().item(j)).getAttribute("w:val");
//                                if (((Element) node.getChildNodes().item(j)).getTagName().equalsIgnoreCase("w:basedOn"))
//                                    numericStyle = ((Element) node.getChildNodes().item(j)).getAttribute("w:val");
                            }
                        }
                    }
                }

            } while (styleName != null && !"".equals(styleName));// || !"".equals(numericStyle));
        }

        return result;
    }

    public static boolean replaceStyles(WordDocument wordLeft, WordDocument wordRight, Map<String,String> replaceMap)
    {
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document oldStylesDoc = builder.parse(wordLeft.getStyles().toFile());
            Document newStylesDoc = builder.parse(wordRight.getStyles().toFile());

            Map<String,String> availableStyles = getStyles(oldStylesDoc);
            Set<String> stylesForReplace = new HashSet<>(replaceMap.values());

            stylesForReplace.addAll(getParentStyles(stylesForReplace, newStylesDoc));

            deleteStyles(wordLeft, new ArrayList<>(stylesForReplace));

            NodeList nodeList = newStylesDoc.getDocumentElement().getChildNodes();

            for (String s : stylesForReplace) {
                if ("".equals(s))
                    continue;

                Node node = null;

                for (int i = 0; i < nodeList.getLength(); i++) {
                    if (((Element) nodeList.item(i)).getTagName().equalsIgnoreCase("w:style")) {
                        if (((Element) nodeList.item(i).getFirstChild()).getAttribute("w:val").equalsIgnoreCase(s)) {
                            node = nodeList.item(i);
                            break;
                        }
                    }
                }

                if (node != null) {
                    Node newNode = oldStylesDoc.importNode(node, true);
                    oldStylesDoc.getDocumentElement().appendChild(newNode);
                    availableStyles.put(((Element) node.getFirstChild()).getAttribute("w:val"), ((Element) node).getAttribute("w:styleId"));
                }
            }

            oldStylesDoc.normalize();

            DOMSource domSource = new DOMSource(oldStylesDoc);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer transformer = tf.newTransformer();
            transformer.transform(domSource, result);

            saveDoc(wordLeft.getStyles(), writer.toString());

            Document doc = builder.parse(wordLeft.getDocument().toFile());

            Map<String, String> newReplaceMap = new HashMap<>();

            for (Map.Entry<String, String> entry : replaceMap.entrySet())
            {
                newReplaceMap.put(availableStyles.get(entry.getKey()), availableStyles.get(entry.getValue()));
            }

            NodeList nodes = doc.getDocumentElement().getChildNodes();

            ArrayList<String> replacedStyles = new ArrayList<>(newReplaceMap.keySet());

            for (String s : replacedStyles)
            {
                recursiveReplace(nodes, newReplaceMap, s);
            }

            doc.normalize();

            DOMSource domSource1 = new DOMSource(doc);
            StringWriter writer1 = new StringWriter();
            StreamResult result1 = new StreamResult(writer1);
            TransformerFactory tf1 = TransformerFactory.newInstance();
            Transformer transformer1 = tf1.newTransformer();
            transformer1.transform(domSource1, result1);

            saveDoc(wordLeft.getDocument(), writer1.toString());

            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public static Map<String,String> getStyles(WordDocument wordDocument) throws Exception
    {
        Map<String,String> result = new HashMap<>();

        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(wordDocument.getStyles().toFile());

        NodeList nodeList = document.getDocumentElement().getChildNodes();
        Node name;

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            if (((Element) nodeList.item(i)).getTagName().equalsIgnoreCase("w:style")
                    && (((Element) nodeList.item(i)).getAttribute("w:type").equalsIgnoreCase("paragraph")
                    || ((Element) nodeList.item(i)).getAttribute("w:type").equalsIgnoreCase("character"))) {
                name = nodeList.item(i).getFirstChild();
                result.put(((Element) name).getAttribute("w:val"), ((Element) nodeList.item(i)).getAttribute("w:styleId"));
            }
        }

        document.normalize();

        return result;
    }

    public static WordDocument createWordDocument(Path word)
    {
        Path document = Paths.get(word.toString() + "/word/document.xml");
        Path styles = Paths.get(word.toString() + "/word/styles.xml");
        Path stylesWithEffects = Paths.get(word.toString() + "/word/stylesWithEffects.xml");

        return new WordDocument(styles, document, stylesWithEffects);
    }

    public static Map<String,String> deleteStyles(WordDocument wordDocument, List<String> listForDelete) throws Exception
    {

        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(wordDocument.getStyles().toFile());
        Document document2 = builder.parse(wordDocument.getStylesWithEffects().toFile());

        removeStylesFromXML(document, listForDelete);

        DOMSource domSource = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);

        saveDoc(wordDocument.getStyles(), writer.toString());

        removeStylesFromXML(document2, listForDelete);

        DOMSource domSource1 = new DOMSource(document2);
        StringWriter writer1 = new StringWriter();
        StreamResult result1 = new StreamResult(writer1);
        TransformerFactory tf1 = TransformerFactory.newInstance();
        Transformer transformer1 = tf1.newTransformer();
        transformer1.transform(domSource1, result1);

        saveDoc(wordDocument.getStylesWithEffects(), writer1.toString());

        return getStyles(document);
    }

    public static Map<String,String> removeUnUsedStyles(WordDocument wordDocument) throws Exception
    {
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(wordDocument.getDocument().toFile());
        Document stylesDoc = builder.parse(wordDocument.getStyles().toFile());

        List<String> usedStyles = new ArrayList<>();

        NodeList nodes = doc.getDocumentElement().getChildNodes();

        getUsedStyles(nodes, usedStyles);

        Map<String,String> forDelete = getStyles(stylesDoc);

        Set<String> tempList = new HashSet<>();
        Map<String, String> temp = new HashMap<>();
        temp.putAll(forDelete);

        for (String s : usedStyles)
        {
            for (Map.Entry<String, String> map : temp.entrySet())
            {
                if (map.getValue().equalsIgnoreCase(s)) {
                    tempList.add(map.getKey());
                    forDelete.remove(map.getKey());
                    break;
                }
            }
        }

        tempList.addAll(getParentStyles(tempList, stylesDoc));

        for (String s : tempList)
            forDelete.remove(s);

        return deleteStyles(wordDocument, new ArrayList<>(forDelete.keySet()));
    }

    private static void getUsedStyles(NodeList nodes, List<String> usedStyles)
    {
        for (int i = 0; i < nodes.getLength(); i++)
        {
            Node node = nodes.item(i);

            if (node == null)
                continue;

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                if (((Element) node).getTagName().equalsIgnoreCase("w:pStyle") && !usedStyles.contains(((Element) node).getAttribute("w:val")))
                    usedStyles.add(((Element) node).getAttribute("w:val"));
            }

            if (node.hasChildNodes())
                getUsedStyles(node.getChildNodes(), usedStyles);
        }
    }

    private static void removeStylesFromXML(Document doc, List<String> listForDelete)
    {
        NodeList nodeList = doc.getDocumentElement().getChildNodes();
        Node name;

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            if (((Element) nodeList.item(i)).getTagName().equalsIgnoreCase("w:style")
                    && (((Element) nodeList.item(i)).getAttribute("w:type").equalsIgnoreCase("paragraph")
                    || ((Element) nodeList.item(i)).getAttribute("w:type").equalsIgnoreCase("character"))) {
                name = nodeList.item(i).getFirstChild();
                if (listForDelete.contains(((Element) name).getAttribute("w:val"))) {
                    nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
                    i = i - 1;
                }
            }
        }
    }

    private static void recursiveReplace( NodeList nodes, Map<String,String> replaceMap, String styleId)
    {
        for (int i = 0; i < nodes.getLength(); i++)
        {
            Node node = nodes.item(i);

            if (node == null)
                continue;

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                if (((Element) node).getTagName().equalsIgnoreCase("w:pStyle")) {
                    if (((Element) node).getAttribute("w:val").equalsIgnoreCase(styleId))
                        ((Element) node).setAttribute("w:val", replaceMap.get(styleId));
                }
            }

            if (node.hasChildNodes())
                recursiveReplace(node.getChildNodes(), replaceMap, styleId);
        }
    }

    private static void saveDoc(Path path, String data) throws IOException
    {
        if(data == null || "".equals(data))
            return;

        Files.delete(path);
        Files.createFile(path);

        List<String> strings = Arrays.asList(data.split("\n"));

        Files.write(path, strings);
    }

    private static Map<String, String> getStyles(Document document) throws IOException
    {
        Map<String, String> result = new HashMap<>();

        NodeList nodeList = document.getDocumentElement().getChildNodes();
        Node name;

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            if (((Element) nodeList.item(i)).getTagName().equalsIgnoreCase("w:style")
                    && (((Element) nodeList.item(i)).getAttribute("w:type").equalsIgnoreCase("paragraph")
                    || ((Element) nodeList.item(i)).getAttribute("w:type").equalsIgnoreCase("character"))) {
                name = nodeList.item(i).getFirstChild();
                result.put(((Element) name).getAttribute("w:val"), ((Element) nodeList.item(i)).getAttribute("w:styleId"));
            }
        }

        document.normalize();

        return result;
    }
}
