package model;

import java.nio.file.Path;

/**
 * Created by romanletun on 26.05.2016.
 */
public class WordDocument
{
    private Path styles;
    private Path document;
    private Path stylesWithEffects;

    public WordDocument(Path styles, Path document, Path webStyles)
    {
        this.styles = styles;
        this.document = document;
        this.stylesWithEffects = webStyles;
    }

    public Path getStyles() {
        return styles;
    }


    public Path getDocument() {
        return document;
    }


    public Path getStylesWithEffects() {
        return stylesWithEffects;
    }
}
