package model;

import model.archiver.Archiver;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by romanletun on 26.05.2016.
 * Хранит ссылку на редактируемый word (левый файл), на Word донор стилей (правый файл).
 * Перед работы с Word разархивирует его во временный каталог, с помощью класса Archiver
 * Вся работа с xml-файлами word документа выполняются абстрактным классом WordParser
 */
public class Model
{
    private Path wordLeftFile; //Путь к левому файлу Word
    private Path wordRightFile; //Путь к правому файлу Word
    private Path extractedLeftWord; //Путь к каталогу с разорхивированными файлами левого документа Word
    private Path extractedRightWord; //Путь к каталогу с разорхивированными файлами правого документа Word
    private List<String> currentLeftStyles = new ArrayList<>(); //Хранятся стили из левого документа Word
    private List<String> currentRightStyles = new ArrayList<>(); //Хранятся стили из правого документа Word

    //Сериализует конфигурационный файл в xml
    public void saveConfig(Map<String, String> replaceMap, List<String> styles, File file) throws Exception
    {
        ReplaceConfig replaceConfig = new ReplaceConfig();
        replaceConfig.setReplacedMap(replaceMap);
        //replaceConfig.setUsedStyles(styles);

        JAXBContext context = JAXBContext.newInstance(ReplaceConfig.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        marshaller.marshal(replaceConfig, file);
    }

    //Десериализация файла конфига
    public ReplaceConfig importConfig(File file) throws Exception
    {
        JAXBContext context = JAXBContext.newInstance(ReplaceConfig.class);

        Unmarshaller unmarshaller = context.createUnmarshaller();

        return (ReplaceConfig) unmarshaller.unmarshal(file);
    }

    //Удаление неиспользуемых стилей из документа
    public List<String> removeUnUsedStyles() throws Exception
    {
        WordDocument leftDoc = WordParser.createWordDocument(extractedLeftWord);
        List<String> styles = new ArrayList<>(WordParser.removeUnUsedStyles(leftDoc).keySet());

        currentLeftStyles.clear();
        currentLeftStyles.addAll(styles);

        return styles;
    }

    //Заменяет стили
    public boolean replaceStyles(Map<String, String> replaceMap)
    {
        WordDocument rightDoc = WordParser.createWordDocument(extractedLeftWord);
        WordDocument leftDoc = WordParser.createWordDocument(extractedRightWord);

        return WordParser.replaceStyles(rightDoc, leftDoc, replaceMap);

    }

    //Получает List стилей из левого файла word
    public List<String> getLeftStyles() throws Exception
    {
        WordDocument leftDoc = WordParser.createWordDocument(extractedLeftWord);
        List<String> styles = new ArrayList<>(WordParser.getStyles(leftDoc).keySet());

        currentLeftStyles.clear();
        currentLeftStyles.addAll(styles);

        return styles;
    }

    //Удаляет стили из левого файла word
    //List<String> styles - имена стилей для удаления
    public List<String> deleteStyles(List<String> styles) throws Exception
    {
        WordDocument leftDoc = WordParser.createWordDocument(extractedLeftWord);
        List<String> result = new ArrayList<>(WordParser.deleteStyles(leftDoc, styles).keySet());

        currentLeftStyles.clear();
        currentLeftStyles.addAll(result);

        return result;
    }

    public void selectLFile(Path word) throws Exception
    {
        wordLeftFile = word;

        extractedLeftWord = Archiver.extract(word);
    }

    public void saveFile() throws Exception
    {
        Archiver.zip(extractedLeftWord, wordLeftFile);
    }

    public boolean isFileSelected()
    {
        return wordLeftFile != null;
    }

    //Right File
    public List<String> getRightStyles() throws Exception
    {
        WordDocument rigthDoc = WordParser.createWordDocument(extractedRightWord);
        List<String> styles = new ArrayList<>(WordParser.getStyles(rigthDoc).keySet());

        currentRightStyles.clear();
        currentRightStyles.addAll(styles);

        return styles;
    }

    public void selectRightFile(Path word) throws Exception
    {
        wordRightFile = word;

        extractedRightWord = Archiver.extract(word);
    }

    public boolean isRightFileSelected()
    {
        return wordRightFile != null;
    }

    public List<String> getCurrentLeftStyles() {
        return currentLeftStyles;
    }

    public List<String> getCurrentRightStyles() {
        return currentRightStyles;
    }
}
