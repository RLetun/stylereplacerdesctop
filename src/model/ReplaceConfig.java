package model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by romanletun on 31.05.2016.
 */
@XmlRootElement
@XmlType(name = "ReplaceConfig")
public class ReplaceConfig
{
    private Map<String, String> replacedMap = new HashMap<>();
    //private List<String> usedStyles = new ArrayList<>();

    public ReplaceConfig()
    {
    }

//    public List<String> getUsedStyles() {
//        return usedStyles;
//    }
//
//    public void setUsedStyles(List<String> usedStyles) {
//        this.usedStyles = usedStyles;
//    }

    public Map<String, String> getReplacedMap() {
        return replacedMap;
    }

    public void setReplacedMap(Map<String, String> replacedMap) {
        this.replacedMap = replacedMap;
    }
}
