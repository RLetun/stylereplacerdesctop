package model.archiver;

/**
 * Created by romanletun on 26.05.2016.
 */

import java.nio.file.Files;
import java.nio.file.Path;

public class Archiver
{
    public static Path extract(Path path) throws Exception
    {
        ZipFileManager zipFileManager = new ZipFileManager(path);

        Path destinationPath = Files.createTempDirectory("");

        zipFileManager.extractAll(destinationPath);

        return destinationPath;
    }

    public static void zip(Path pathToFiles, Path pathToZip) throws Exception
    {
        ZipFileManager zipFileManager =  new ZipFileManager(pathToZip);

        zipFileManager.createZip(pathToFiles);
    }
}
