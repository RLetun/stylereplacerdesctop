package controller;

import model.Model;
import model.ReplaceConfig;
import view.Side;
import view.View;

import javax.swing.*;
import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by romanletun on 26.05.2016.
 */
public class Controller
{
    private Model model;
    private View view;

    public Controller()
    {
        model = new Model();
        view = new View(this);

        view.init();
    }

    public static void main(String[] args)
    {
        Controller controller = new Controller();
    }

    //Импорт конфиг файла
    public void importConfig()
    {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new XMLFileFilter());

        if (fileChooser.showSaveDialog(view) == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();

            ReplaceConfig config;

            try {
                config = model.importConfig(file);
            } catch (Exception e)
            {
                JOptionPane.showMessageDialog(view, "Ошибка при импорте файла настроек.");
                return;
            }

            view.updateTableForImport(config.getReplacedMap());
        }
    }

    //Сохранение конфиг файла
    public void saveConfig(Map<String, String> replaceMap, List<String> styles)
    {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new XMLFileFilter());

        if (fileChooser.showSaveDialog(view) == JFileChooser.APPROVE_OPTION)
        {
            File file = fileChooser.getSelectedFile();

            try {
                model.saveConfig(replaceMap, styles, file);
            }
            catch (Exception e)
            {
                JOptionPane.showMessageDialog(view, "Ошибка при сохранении файла настроек.");
            }
        }
    }

    //Удаление неиспользуемых стилей
    public void removeUnUsedStyles()
    {
        try {
            List<String> usedStyles = model.removeUnUsedStyles();
            view.resetListLeftStyles(usedStyles);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    //Замена стилей
    public void replaceStyles(Map<String, String> replacedMap)
    {
        if (replacedMap.size() == 0)
            return;

        if (!model.isFileSelected() || !model.isRightFileSelected())
            return;

        if (model.replaceStyles(replacedMap))
            JOptionPane.showMessageDialog(view, "Замена стилей произведена успешно!");
        else
            JOptionPane.showMessageDialog(view, "Ошибка при замене стилей.");

        List<String> deleteList = new ArrayList<>(replacedMap.keySet());

        deleteStyles(deleteList);
    }

    //Сохранение левого документа
    public void saveFile()
    {
        try {
            if (model.isFileSelected())
                model.saveFile();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            JOptionPane.showMessageDialog(view, "Ошибка при сохранении файла!");
            return;
        }

        JOptionPane.showMessageDialog(view, "Файл сохранён");
    }

    //Удаление стилей
    public void deleteStyles(List<String> stylesForDelete)
    {
        try
        {
            if (!model.isFileSelected())
                return;

            List<String> styles = model.deleteStyles(stylesForDelete);
            view.resetListLeftStyles(styles);

        }
        catch (Exception e)
        {
            //JOptionPane.showMessageDialog(view, io.getMessage() + "\n" + io.getStackTrace());
            e.printStackTrace();
        }
    }

    //Получить стили документа Word
    //В зависимоти от параметра Side button вернёт стили левого или правого документа
    public void getStylesFromModel(Side button)
    {
        try
        {
            if (button == Side.LEFT)
            {
                List<String> styles = model.getLeftStyles();
                view.resetListLeftStyles(styles);
            }
            else
            {
                List<String> styles = model.getRightStyles();
                view.resetListRightStyles(styles);
            }
        }
        catch (Exception e)
        {
            //JOptionPane.showMessageDialog(view, e.getMessage() + "\n" + e.getStackTrace());
            e.printStackTrace();
        }
    }

    //Открытие документа Word
    public void selectFile(Side button)
    {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new WordFileFilter());
        Path file;

        if(fileChooser.showOpenDialog(view) == JFileChooser.APPROVE_OPTION)
        {
            file = fileChooser.getSelectedFile().toPath();
            try
            {
                if (button == Side.LEFT)
                    model.selectLFile(file);
                else
                    model.selectRightFile(file);
            }
            catch (Exception e)
            {
                //JOptionPane.showMessageDialog(view, e.getMessage() + "\n" + e.getStackTrace());
                e.printStackTrace();
            }
        }
    }

    public void exit()
    {
        System.exit(0);
    }

    public List<String> getRightStyles()
    {
        return model.getCurrentLeftStyles();
    }

    public List<String> getLeftStyles()
    {
        return model.getCurrentRightStyles();
    }
}
