package controller;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by romanletun on 31.05.2016.
 */
public class XMLFileFilter extends FileFilter
{
    @Override
    public boolean accept(File f)
    {
        String name = f.getName().toLowerCase();
        return name.endsWith(".xml") || f.isDirectory();
    }

    @Override
    public String getDescription()
    {
        return "Файлы XML";
    }
}
