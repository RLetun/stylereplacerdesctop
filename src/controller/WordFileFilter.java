package controller;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Created by romanletun on 26.05.2016.
 */
public class WordFileFilter extends FileFilter
{
    @Override
    public boolean accept(File f)
    {
        String name = f.getName().toLowerCase();
        return name.endsWith(".docx") || name.endsWith(".doc") || name.endsWith(".dotx")|| name.endsWith(".dot") || f.isDirectory();
    }

    @Override
    public String getDescription()
    {
        return "Документы Word";
    }
}
